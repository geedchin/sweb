package strutil

func BytesOrString(obj interface{}) (string, bool) {
	if result, ok := obj.([]byte); ok {
		return string(result), true
	}
	if result, ok := obj.(string); ok {
		return result, true
	}
	return "", false
}
