package crash

import (
	"fmt"
	"runtime/debug"
)

func Handler(f func()) {
	if r := recover(); r != nil {
		if f != nil {
			f()
		}
		fmt.Println(string(debug.Stack()))
	}
}
